# Base Project

This is base iOS project using MVVM architecture.

### Techniques:
  - MVVM with RxSwift
  - Dependences Injection with Swinject
  - Mutiple targets and environments
  - SignIn with Facebook, Google and Apple
  - Firebase integration (FCM)
  - Support Branch.io (deeplink)
  - Some common utils

> This is the basic configurations to help us implement easier, don't contain enough information to run correctly.

### How to use

Starting with **master** branch code and checkout config branch which you want. Or
Use **develop** branch with all common features.

Steps:
* Clone project and checkout correct branch
* Rename project and scheme if needed
* Run *pod install* to install all libs
* Register Google, Firebase, Facebook, Apple project and update their configurations to your project
* Update API endpoints for each environments: DEV, STAGING and PRODUCTION

### Common Libs

This project is currently using some common libs. Instructions on how to use them in your own application are linked below.

| Plugin | Used to |  README |
| ------ | ------- | ------ |
| RxSwift | Asynchronous handler  | https://github.com/ReactiveX/RxSwift/blob/main/README.md |
| RxCocoa | Rx for iOS Cocoa  | https://github.com/ReactiveX/RxSwift/blob/main/README.md |
| RxAlamofire | Rx + Alamofire for API handler | https://github.com/RxSwiftCommunity/RxAlamofire/blob/main/README.md |
| ReachabilitySwift | Check network status | https://github.com/ashleymills/Reachability.swift/blob/master/README.md |
| Swinject | Dependency injection | https://github.com/Swinject/Swinject/blob/master/README.md |
| SwinjectAutoregistration | Extension of Swinject that allows to automatically register your components | https://github.com/Swinject/SwinjectAutoregistration/blob/master/README.md |

### Todos

 - Comming soon

License
----

MIT
