//
//  Container+Coodinators.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Swinject
import SwinjectAutoregistration

extension Container {
    func registerCoordinators() {
        autoregister(InitCoordinator.self, initializer: InitCoordinator.init)
        autoregister(HomeCoordinator.self, initializer: HomeCoordinator.init)
    }
}
