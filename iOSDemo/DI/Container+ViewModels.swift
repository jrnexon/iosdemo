//
//  Container+ViewModels.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Swinject
import SwinjectAutoregistration

extension Container {
    func registerViewModels() {
        autoregister(InitViewModel.self, initializer: InitViewModel.init)
        autoregister(HomeViewModel.self, initializer: HomeViewModel.init)
    }
}
