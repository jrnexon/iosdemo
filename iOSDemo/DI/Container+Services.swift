//
//  Container+Services.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Swinject
import SwinjectAutoregistration

extension Container {
    func registerServices() {
        self.autoregister(OAuthHandler.self, initializer: OAuthHandler.init).inObjectScope(.container)
        self.autoregister(APIService.self, initializer: APIService.init).inObjectScope(.container)
        self.autoregister(Repository.self, initializer: RepositoryImp.init).inObjectScope(.container)
    }
}
