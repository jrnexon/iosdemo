//
//  Container+ViewControllers.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Swinject
import SwinjectAutoregistration

extension Container {
    func registerViewControllers() {
        autoregister(HomeViewController.self, initializer: HomeViewController.init)
    }
}
