//
//  Container+Register.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Swinject

extension Container {
    func registerDependencies() {
        registerServices()
        registerViewModels()
        registerCoordinators()
        registerViewControllers()
    }
}
