//
//  CommonResponse.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Foundation

struct CommonResponse<T: Decodable>: Decodable {
    let data: T?
    let error: String?
}
