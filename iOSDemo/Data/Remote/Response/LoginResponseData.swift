//
//  LoginResponseData.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Foundation

struct LoginResponseData: Decodable {
    let accessToken: String?
    let refreshToken: String?
}
