//
//  UDStorage.swift
//  iOSDemo
//
//  Created by Toan Truong on 01/12/2020.
//

import Foundation

class UDStorage: UserDefaults {
    
    static let shared = UDStorage()
    
    static let PREFIX = "APP_"
    
    private let kAccessToken = UDStorage.PREFIX + "ACCESS_TOKEN"
    var accessToken: String? {
        get {
            return string(forKey: kAccessToken)
        }
        set {
            set(newValue, forKey: kAccessToken)
        }
    }
    
    private let kRefreshToken = UDStorage.PREFIX + "ACCESS_TOKEN"
    var refreshToken: String? {
        get {
            return string(forKey: kRefreshToken)
        }
        set {
            set(newValue, forKey: kRefreshToken)
        }
    }
}
