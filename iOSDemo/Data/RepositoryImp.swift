//
//  RepositoryImp.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Foundation
import RxSwift

class RepositoryImp: Repository {
    
    private let apiService: APIService
    
    init(apiService: APIService) {
        self.apiService = apiService
        print("DI:Init RepositoryImp with apiService = \(apiService)")
    }
    
    func login(username: String, password: String) -> Observable<CommonResponse<LoginResponseData>?> {
        return apiService.login(username: username, password: password)
    }
}
