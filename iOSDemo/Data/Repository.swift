//
//  Repository.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Foundation
import RxSwift

protocol Repository {
    // MARK: - Authentication
    func login(username: String, password: String) -> Observable<CommonResponse<LoginResponseData>?>
}
