//
//  APIService+Authentication.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Foundation
import RxSwift

extension APIService {
    func login(username: String, password: String) -> Observable<CommonResponse<LoginResponseData>?> {
        return post(API.Endpoints.Authentication.signIn, parameters: ["username": username, "password": password]).apiMap()
    }
}
