//
//  API.swift
//  iOSDemo
//
//  Created by Toan Truong on 01/12/2020.
//

import Foundation

struct API {
    static let BASE_HOST = "https://localhost.com"
    struct Endpoints {
        struct Authentication {
            static let signIn = API.BASE_HOST + "/login"
            static let refreshToken = API.BASE_HOST + "/refresh_token"
        }
    }
}
