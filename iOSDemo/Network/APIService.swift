//
//  APIService.swift
//  iOSDemo
//
//  Created by Toan Truong on 01/12/2020.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import Reachability

class APIService {
    
    let sessionManager: Session
    
    init(oAuthHandler: OAuthHandler? = nil) {
        debugPrint("DI:Init APIService with oAuthHandler = \(String(describing: oAuthHandler))")
        
        let configuration = URLSessionConfiguration.default
        configuration.headers = .default
        configuration.timeoutIntervalForRequest = 20
        configuration.timeoutIntervalForResource = 20
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        
        sessionManager = Session(configuration: configuration, interceptor: oAuthHandler)
    }
    
    static let defaultError = NSError(domain: "Network", code: -1, userInfo: [NSLocalizedDescriptionKey: "Something went wrong!"])
    static let timeoutError = NSError(domain: "Network", code: -1, userInfo: [NSLocalizedDescriptionKey: "Request timed out"])
    
    // MARK: - RESTful methods
    open func get(_ url: URLConvertible,
                  parameters: [String: Any]? = nil,
                  headers: [String: String]? = nil) -> Observable<(HTTPURLResponse, Data)> {
        return request(.get, url, parameters: parameters, encoding: URLEncoding.default, headers: headers)
    }
    
    open func post(_ url: URLConvertible,
                   parameters: [String: Any]? = nil,
                   headers: [String: String]? = nil) -> Observable<(HTTPURLResponse, Data)> {
        return request(.post, url, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
    }
    
    open func put(_ url: URLConvertible,
                  parameters: [String: Any]? = nil,
                  headers: [String: String]? = nil) -> Observable<(HTTPURLResponse, Data)> {
        return request(.put, url, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
    }
    
    open func patch(_ url: URLConvertible,
                  parameters: [String: Any]? = nil,
                  headers: [String: String]? = nil) -> Observable<(HTTPURLResponse, Data)> {
        return request(.patch, url, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
    }
    
    open func delete(_ url: URLConvertible,
                     parameters: [String: Any]? = nil,
                     headers: [String: String]? = nil) -> Observable<(HTTPURLResponse, Data)> {
        return request(.delete, url, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
    }
    
    // MARK: - Common
    open func request(_ method: Alamofire.HTTPMethod,
                      _ url: URLConvertible,
                      parameters: [String: Any]? = nil,
                      encoding: ParameterEncoding = URLEncoding.default,
                      headers: [String: String]? = nil) -> Observable<(HTTPURLResponse, Data)> {
            
        return sessionManager.rx.request(method, url, parameters: parameters, encoding: encoding, headers: HTTPHeaders(headers ?? [:]))
            .handleErrors()
            .observeOn(MainScheduler.instance)
    }
    
    // MARK: - Upload
    open func uploadFiles(_ url: URLConvertible,
                          files: [URL],
                          parameters: [String: Any]? = nil, headers: [String: String]? = nil) -> Observable<(HTTPURLResponse, Data)> {
        return sessionManager.rx.upload(multipartFormData: { multipartFormData in
            for i in 0..<files.count {
                let file = files[i]
                multipartFormData.append(file, withName: "file")
            }
            
            for (key, value) in parameters ?? [:] {
                multipartFormData.append((String(describing: value).data(using: .utf8))!, withName: key)
            }
        }, to: url, method: .post, headers: HTTPHeaders(headers ?? [:]))
        .handleErrors()
        .observeOn(MainScheduler.instance)
    }
}

// MARK: - Utils

extension Observable where Element == (HTTPURLResponse, Data) {
    func apiMap<T: Decodable>() -> Observable<T?> {
        return map {
            do {
                return try JSONDecoder().decode(T.self, from: $0.1)
            } catch {
                debugPrint("PARSE_ERROR: " + error.localizedDescription)
                return nil
            }
        }
    }
}

extension Observable where Element == DataRequest {
    func handleErrors() -> Observable<(HTTPURLResponse, Data)> {
        return flatMap {
            $0.validate(statusCode: [200..<401, 402..<501].joined())
                .validate(contentType: ["application/json"])
                .rx.responseData()
        }
        .catchError({ (error) -> Observable<(HTTPURLResponse, Data)> in
            let nsError = (error as NSError)
            if nsError.domain == NSURLErrorDomain && nsError.code == NSURLErrorNotConnectedToInternet {
                throw NSError(domain: kCFErrorDomainCFNetwork as String, code: NSURLErrorNotConnectedToInternet, userInfo: [NSLocalizedDescriptionKey : "No internet connection."])
            } else if nsError.code == NSURLErrorTimedOut {
                throw APIService.timeoutError
            } else {
                throw APIService.defaultError
            }
        })
    }
}


extension Observable where Element == UploadRequest {
    func handleErrors() -> Observable<(HTTPURLResponse, Data)> {
        return flatMap {
            $0.validate(statusCode: [200..<401, 402..<501].joined())
                .validate(contentType: ["application/json"])
                .rx.responseData()
        }
        .catchError({ (error) -> Observable<(HTTPURLResponse, Data)> in
            let nsError = (error as NSError)
            if nsError.domain == NSURLErrorDomain && nsError.code == NSURLErrorNotConnectedToInternet {
                throw NSError(domain: kCFErrorDomainCFNetwork as String, code: NSURLErrorNotConnectedToInternet, userInfo: [NSLocalizedDescriptionKey : "No internet connection."])
            } else if nsError.code == NSURLErrorTimedOut {
                throw APIService.timeoutError
            } else {
                throw APIService.defaultError
            }
        })
    }
}
