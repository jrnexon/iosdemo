//
//  Encodable+Dictionary.swift
//  iOSDemo
//
//  Created by Toan Truong on 14/12/2020.
//

import Foundation

extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}

extension Dictionary {
    func jsonString() -> String? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: []) {
            let jsonString = String(data: jsonData, encoding: .utf8)
            return jsonString
        }
        return nil
    }
}
