//
//  Date+Utils.swift
//  iOSDemo
//
//  Created by Toan Truong on 09/12/2020.
//

import Foundation

extension Date {
    
    static let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
    static let defaultDateTimeFormat = "MM/dd/yyyy"
    static let defaultApiDateTimeFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    
    var next: Date {
        var c = Calendar.current.dateComponents(in: .current, from: self)
        c.day = c.day! + 1
        return Calendar(identifier: .gregorian).date(from: c)!
    }
    
    var prev: Date {
        var c = Calendar.current.dateComponents(in: .current, from: self)
        c.day = c.day! - 1
        return Calendar(identifier: .gregorian).date(from: c)!
    }
    
    func from(_ string: String?, format: String = defaultDateTimeFormat, locale: Locale = enUSPosixLocale) -> Date? {
        guard let string = string else {
            return nil
        }
        
        let f = DateFormatter()
        f.dateFormat = format
        return f.date(from: string)
    }
    
    func from(year: Int, month: Int, day: Int) -> Date? {
        var c = DateComponents()
        c.year = year
        c.month = month
        c.day = day
        
        return Calendar(identifier: .gregorian).date(from: c)
    }
    
    func formattedString(format: String = defaultDateTimeFormat, locale: Locale = enUSPosixLocale) -> String {
        let f = DateFormatter()
        f.dateFormat = format
        f.locale = locale
        return f.string(from: self)
    }
    
    func sameWith(_ date: Date) -> Bool {
        let components1 = Calendar.current.dateComponents(in: .current, from: self)
        let components2 = Calendar.current.dateComponents(in: .current, from: date)
        
        return components1.day == components2.day
            && components1.month == components2.month
            && components1.year == components2.year
    }
}
