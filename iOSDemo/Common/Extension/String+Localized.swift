//
//  String+Localized.swift
//  iOSDemo
//
//  Created by Toan Truong on 09/12/2020.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
}
