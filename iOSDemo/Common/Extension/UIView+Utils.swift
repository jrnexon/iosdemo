//
//  UIView+Utils.swift
//  iOSDemo
//
//  Created by Toan Truong on 14/12/2020.
//

import UIKit

extension UIView {
    func applyGradient(colours: [UIColor], locations: [NSNumber]? = nil, startPoint: CGPoint? = nil, endPoint: CGPoint? = nil, cornerRadius: CGFloat = 0) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.cornerRadius = cornerRadius
        gradient.masksToBounds = true
        if let startPoint = startPoint {
            gradient.startPoint = startPoint
        }
        if let endPoint = endPoint {
            gradient.endPoint = endPoint
        }
        self.layer.sublayers?.forEach({ (layer) in
            if layer is CAGradientLayer {
                layer.removeFromSuperlayer()
            }
        })
        self.layer.insertSublayer(gradient, at: 0)
    }
    

    func horizontalGradientDisable(colours: [UIColor] = [UIColor(named: "Disable")!,UIColor(named: "Disable")!], cornerRadius: CGFloat = 0) {
        if self is UIButton {
            (self as! UIButton).setGradientBackgroundColors(colours, direction: .toRight, for: .normal)
            (self as! UIButton).setGradientBackgroundColors(colours, direction: .toRight, for: .disabled)
        } else {
            clearGradient()
            applyGradient(colours: colours,startPoint: CGPoint(x: 0, y: 0.5),endPoint:  CGPoint(x: 1, y: 0.5), cornerRadius: cornerRadius)
        }
    }
    
    func horizontalGradient(colours: [UIColor] = [UIColor(named: "Primary")!,UIColor(named: "Secondary")!], cornerRadius: CGFloat? = nil) {
        if self is UIButton {
            (self as! UIButton).setGradientBackgroundColors(colours, direction: .toRight, for: .normal)
            (self as! UIButton).setGradientBackgroundColors(colours, direction: .toRight, for: .disabled)
        } else {
            clearGradient()
            applyGradient(colours: colours,startPoint: CGPoint(x: 0, y: 0.5),endPoint:  CGPoint(x: 1, y: 0.5), cornerRadius: cornerRadius ?? layer.cornerRadius)
        }
    }
    
    func verticalGradient(colours: [UIColor] = [UIColor(named: "Primary")!,UIColor(named: "Secondary")!], cornerRadius: CGFloat = 0) {
        if self is UIButton {
            (self as! UIButton).setGradientBackgroundColors(colours, direction: .toBottom, for: .normal)
            (self as! UIButton).setGradientBackgroundColors(colours, direction: .toBottom, for: .disabled)
        } else {
            clearGradient()
            applyGradient(colours: colours,startPoint: CGPoint(x: 0.5, y: 0),endPoint:  CGPoint(x: 0.5, y: 1), cornerRadius: cornerRadius)
        }
    }
    
    func clearGradient() -> Void {
        
        self.layer.sublayers?.forEach({ (layer) in
            if layer.isKind(of: CAGradientLayer.self) {
                layer.removeFromSuperlayer()
            }
        })
    }
    
    func showAlphaHighlight() {
        let lastAlpha = self.alpha
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.alpha = lastAlpha * 0.8
        }) { [weak self](_) in
            UIView.animate(withDuration: 0.1) { [weak self] in
                self?.alpha = lastAlpha
            }
        }
    }
    
    func fillParent(_ parent: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: parent.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: parent.bottomAnchor).isActive = true
        self.leftAnchor.constraint(equalTo: parent.leftAnchor).isActive = true
        self.rightAnchor.constraint(equalTo: parent.rightAnchor).isActive = true
    }
    
    func addBottomShadow(shadowOpacity: Float = 0.05) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 1
        layer.shadowOpacity = shadowOpacity
        layer.shadowOffset = CGSize(width: 0, height: 1)
    }
    
    func addTopShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 1
        layer.shadowOpacity = 0.05
        layer.shadowOffset = CGSize(width: 0, height: -1)
    }
    
    func rounded(color: UIColor, cornerRadius: CGFloat = 8.0, borderWidth: CGFloat = 1.0) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
    }
}


extension UIButton {
    func setGradientBackgroundColors(_ colors: [UIColor], direction: DTImageGradientDirection, for state: UIControl.State) {
        if colors.count > 1 {
            // Gradient background
            setBackgroundImage(UIImage(size: CGSize(width: 1, height: 1), direction: direction, colors: colors), for: state)
        }
        else {
            if let color = colors.first {
                // Mono color background
                setBackgroundImage(UIImage(color: color, size: CGSize(width: 1, height: 1)), for: state)
            }
            else {
                // Default background color
                setBackgroundImage(nil, for: state)
            }
        }
    }
}

public enum DTImageGradientDirection {
    case toLeft
    case toRight
    case toTop
    case toBottom
    case toBottomLeft
    case toBottomRight
    case toTopLeft
    case toTopRight
}

public extension UIImage {
    convenience init?(size: CGSize, direction: DTImageGradientDirection, colors: [UIColor]) {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        
        guard let context = UIGraphicsGetCurrentContext() else { return nil } // If the size is zero, the context will be nil.
        
        guard colors.count >= 1 else { return nil } // If less than 1 color, return nil
        
        if colors.count == 1 {
            // Mono color
            let color = colors.first!
            color.setFill()
            
            let rect = CGRect(origin: CGPoint.zero, size: size)
            UIRectFill(rect)
        }
        else {
            // Gradient color
            var locations: [CGFloat] = []
            
            for (index, _) in colors.enumerated() {
                let index = CGFloat(index)
                locations.append(index / CGFloat(colors.count - 1))
            }
            
            guard let gradient = CGGradient(colorSpace: CGColorSpaceCreateDeviceRGB(), colorComponents: colors.compactMap { $0.cgColor.components }.flatMap { $0 }, locations: locations, count: colors.count) else {
                return nil
            }
            
            var startPoint: CGPoint
            var endPoint: CGPoint
            
            switch direction {
            case .toLeft:
                startPoint = CGPoint(x: size.width, y: size.height/2)
                endPoint = CGPoint(x: 0.0, y: size.height/2)
            case .toRight:
                startPoint = CGPoint(x: 0.0, y: size.height/2)
                endPoint = CGPoint(x: size.width, y: size.height/2)
            case .toTop:
                startPoint = CGPoint(x: size.width/2, y: size.height)
                endPoint = CGPoint(x: size.width/2, y: 0.0)
            case .toBottom:
                startPoint = CGPoint(x: size.width/2, y: 0.0)
                endPoint = CGPoint(x: size.width/2, y: size.height)
            case .toBottomLeft:
                startPoint = CGPoint(x: size.width, y: 0.0)
                endPoint = CGPoint(x: 0.0, y: size.height)
            case .toBottomRight:
                startPoint = CGPoint(x: 0.0, y: 0.0)
                endPoint = CGPoint(x: size.width, y: size.height)
            case .toTopLeft:
                startPoint = CGPoint(x: size.width, y: size.height)
                endPoint = CGPoint(x: 0.0, y: 0.0)
            case .toTopRight:
                startPoint = CGPoint(x: 0.0, y: size.height)
                endPoint = CGPoint(x: size.width, y: 0.0)
            }
            
            context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: CGGradientDrawingOptions())
        }
        
        guard let image = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else {
            return nil
        }
        
        self.init(cgImage: image)
        
        defer { UIGraphicsEndImageContext() }
    }
    
    convenience init?(color: UIColor, size: CGSize) {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        let rect = CGRect(origin: CGPoint.zero, size: size)
        UIRectFill(rect)
        
        guard let image = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else {
            return nil
        }
        
        self.init(cgImage: image)
        
        defer { UIGraphicsEndImageContext() }
    }
}
