//
//  UIViewController+Utils.swift
//  iOSDemo
//
//  Created by Toan Truong on 14/12/2020.
//

import UIKit

extension UIViewController {
    
    func add(_ child: UIViewController, viewGroup: UIView? = nil) {
        addChild(child)
        let addingView: UIView = viewGroup ?? view
        addingView.addSubview(child.view)
        child.view.fillParent(addingView)
        child.didMove(toParent: self)
    }
    
    func removeFromParent() {
        guard parent != nil else {
            return
        }
        
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)

        present(viewControllerToPresent, animated: false)
    }

    func dismissDetail(completion: (() -> Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)

        dismiss(animated: false){
            completion?()
        }
    }
}
