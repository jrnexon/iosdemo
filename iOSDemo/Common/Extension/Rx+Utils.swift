//
//  Rx+Utils.swift
//  iOSDemo
//
//  Created by Toan Truong on 14/12/2020.
//

import RxSwift
import RxCocoa

func keyboardHeight() -> Observable<CGFloat> {
    return Observable
            .from([
                NotificationCenter.default.rx.notification(UIWindow.keyboardWillShowNotification)
                    .map { notification -> CGFloat in
                        (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
                    },
                NotificationCenter.default.rx.notification(UIWindow.keyboardWillHideNotification)
                    .map { _ in 0 }
            ])
            .merge()
}

