//
//  CommonUtils.swift
//  iOSDemo
//
//  Created by Toan Truong on 09/12/2020.
//

import Foundation
import UIKit

class CommonUtils {
    
    
    class Image {
        
        static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
            
            let scale = newWidth / image.size.width
            let newHeight = image.size.height * scale
            UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
            image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return newImage!
        }
        
        static func resizeImage(image: UIImage, limitSizeInKB: Int) -> Data? {
            var compression: CGFloat = 1.0
            let maxCompression: CGFloat = 0.1
            
            var imageData = image.jpegData(compressionQuality: compression)
            while (imageData != nil && imageData!.count > limitSizeInKB * 1024 && compression > maxCompression) {
                compression -= 0.1
                imageData = image.jpegData(compressionQuality: compression)
            }
            
            return imageData
        }
    }
}
