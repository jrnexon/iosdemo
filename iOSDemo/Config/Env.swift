//
//  Env.swift
//  iOSDemo
//
//  Created by Toan Truong on 09/12/2020.
//

import Foundation

class Env {
    
    static var shared = Env()
    
    func printEnviroment() {
        #if DEV
        print("Running dev mode...")
        #elseif STAGING
        print("Running staging mode...")
        #else
        print("Running production mode...")
        #endif
    }
    
    var apiHost: String {
        get {
            #if DEV
            return "https://api.reachat.dirox.dev"
            #elseif STAGING
            return "https://api.reachat.dirox.app"
            #else
            return "-"
            #endif
        }
    }
    
    var webHost: String {
        get {
            #if DEV
            return "https://reachat.dirox.dev"
            #elseif STAGING
            return "https://reachat.dirox.app"
            #else
            return "-"
            #endif
        }
    }
    
    var deeplinkHost: String {
        get {
            #if DEV
            return "https://reachatdev.app.link"
            #elseif STAGING
            return "https://reachatstaging.app.link"
            #else
            return "-"
            #endif
        }
    }
    
    var googleConfigFile: String {
        get {
            #if DEV
            return "GoogleService-Info-Dev"
            #elseif STAGING
            return "GoogleService-Info-Staging"
            #else
            return "GoogleService-Info"
            #endif
        }
    }
    
    var googleSignInClientId: String {
        get {
            #if DEV
            return "id1.apps.googleusercontent.com"
            #elseif STAGING
            return "id2.apps.googleusercontent.com"
            #else
            return "-"
            #endif
        }
    }
}
