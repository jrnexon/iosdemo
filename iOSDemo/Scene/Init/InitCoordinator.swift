//
//  InitCoordinator.swift
//  iOSDemo
//
//  Created by Toan Truong on 08/12/2020.
//

import Foundation

class InitCoordinator: BaseCoordinator {
    
    func goHome() {
        let vc = AppDelegate.container.resolve(HomeViewController.self)!
        navigationController?.pushViewController(vc, animated: true)
    }
}
