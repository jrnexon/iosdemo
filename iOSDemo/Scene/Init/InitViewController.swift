//
//  InitViewController.swift
//  iOSDemo
//
//  Created by Toan Truong on 01/12/2020.
//

import UIKit

class InitViewController: UIViewController {
    private var viewModel: InitViewModel = AppDelegate.container.resolve(InitViewModel.self)!
    private var coordinator: InitCoordinator! = AppDelegate.container.resolve(InitCoordinator.self)!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        coordinator.config(with: navigationController)
    }
    
    @IBAction func actionGoHome(_ sender: Any) {
        coordinator.goHome()
    }
}

