//
//  BaseViewModel.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Foundation
import RxSwift

class BaseViewModel {
    
    deinit {
        print("DEINIT - \(String(describing: self))")
    }
    
    let disposeBag = DisposeBag()
    let error = PublishSubject<NSError>()
    let loading = PublishSubject<Bool>()
}
