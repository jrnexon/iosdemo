//
//  BaseCoordinator.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import UIKit

class BaseCoordinator {
    
    deinit {
        print("DEINIT - \(String(describing: self))")
    }
    
    weak var navigationController: UINavigationController?
    
    func config(with navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
    
    func back(animated: Bool = true) {
        navigationController?.popViewController(animated: animated)
    }
    
    func backToRoot(animated: Bool = true) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func back(viewControllerType: AnyClass) {
        var handled = false
        for vc in (navigationController?.viewControllers ?? []).reversed().map({ $0 }) {
            guard !handled else {
                break
            }
            if type(of: vc) == viewControllerType {
                navigationController?.popToViewController(vc, animated: true)
                handled = true
                break
            }
        }
        
        if !handled {
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func back(viewControllerTypes: [AnyClass], willBack: ((AnyClass) -> Void)? = nil) {
        var handled = false
        for vc in (navigationController?.viewControllers ?? []).reversed().map({ $0 }) {
            guard !handled else {
                break
            }
            for viewControllerType in viewControllerTypes {
                if type(of: vc) == viewControllerType {
                    willBack?(viewControllerType)
                    navigationController?.popToViewController(vc, animated: true)
                    handled = true
                    break
                }
            }
        }
        
        if !handled {
            navigationController?.popToRootViewController(animated: true)
        }
    }
}
