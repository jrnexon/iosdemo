//
//  BaseViewController.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import UIKit
import RxSwift

class BaseViewController: UIViewController {
    let disposeBag = DisposeBag()
    private var hasKeyboard = false
    
    deinit {
        print("DEINIT - \(String(describing: self))")
    }
    
    init(nibName: String) {
        super.init(nibName: nibName, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bindRx()
        gestureToDismissKeyboard()
    }
    
    func bindRx() {
        
    }
}

extension BaseViewController {
    
    func showError(title: String = "error".localized, message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: handler))
        present(vc, animated: true)
    }
    
    func showMessage(message: String, handler: (() -> Void)? = nil) {
        let vc = UIAlertController(title: "", message: "\n\n\n\n" + message, preferredStyle: .alert)
        let icon = UIImageView(image: UIImage(named: "ic_success"))
        icon.frame = CGRect(x: 110, y: 20, width: 50, height: 50)
        icon.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        vc.view.addSubview(icon)
        
        present(vc, animated: true) {
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: [.curveEaseInOut], animations: {
                icon.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                vc.dismiss(animated: true) {
                    handler?()
                }
            }
        }
    }
    
    func showConfirm(message: String, mainTitle: String? = "ok".localized, handler: (() -> Void)? = nil) {
        let vc = UIAlertController(title: "", message: message, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: mainTitle, style: .default, handler: { (_) in
            handler?()
        }))
        present(vc, animated: true)
    }
    
    func showConfirm(message: String,mainTitle: String? = "yes".localized, subTitle: String? = "no".localized, handler: ((Bool) -> Void)? = nil) {
        let vc = UIAlertController(title: "", message: message, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: subTitle, style: .default, handler: { (_) in
            handler?(false)
        }))
        vc.addAction(UIAlertAction(title: mainTitle, style: .default, handler: { (_) in
            handler?(true)
        }))
        present(vc, animated: true)
    }
    
    func showPermissonOpenSettings(message: String, handler: ((Bool) -> Void)? = nil) {
        let vc = UIAlertController(title: "grant_permissions".localized, message: message, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: { (_) in
            handler?(false)
        }))
        vc.addAction(UIAlertAction(title: "settings".localized, style: .default, handler: { (_) in
            handler?(true)
        }))
        present(vc, animated: true)
    }
    
    func showWarning(handler: ((Bool) -> Void)? = nil) {
        let vc = UIAlertController(title: "", message: "warning_leave_message".localized, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "stay".localized, style: .default, handler: { (_) in
            handler?(false)
        }))
        vc.addAction(UIAlertAction(title: "leave".localized, style: .default, handler: { (_) in
            handler?(true)
        }))
        present(vc, animated: true)
    }
    
    func showUnderdevelopment() {
        let vc = UIAlertController(title: "Under development", message: "", preferredStyle: .alert)
        
        vc.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
        present(vc, animated: true)
    }
    
}

extension BaseViewController {
    
    
    func gestureToDismissKeyboard () {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        keyboardHeight()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self](height) in
                guard self != nil else { return }
                self!.hasKeyboard = height > 0
                if height > 0 {
                    self!.view.addGestureRecognizer(tap)
                } else {
                    self!.view.removeGestureRecognizer(tap)
                }
            })
            .disposed(by: disposeBag)
    }
    
    @objc func dismissKeyboard() {
        if self.hasKeyboard {
            view.endEditing(true)
        }
    }
}
