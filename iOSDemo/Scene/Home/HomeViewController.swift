//
//  HomeViewController.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import UIKit
import RxSwift

class HomeViewController: BaseViewController {
    private let viewModel: HomeViewModel
    private let coordinator: HomeCoordinator
    
    init(viewModel: HomeViewModel, coordinator: HomeCoordinator) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: "HomeViewController")
        self.title = "home".localized
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        coordinator.config(with: navigationController)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.login(username: "admin", password: "123456")
    }
    
    override func bindRx() {
        viewModel.loading
            .subscribe(onNext: { (loading) in
                print("Loading: \(loading)")
            })
            .disposed(by: disposeBag)
        
        viewModel.error
            .subscribe(onNext: { (error) in
                print("Error: \(error.localizedDescription)")
            })
            .disposed(by: disposeBag)
    }
}
