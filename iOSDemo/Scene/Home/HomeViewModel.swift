//
//  HomeViewModel.swift
//  iOSDemo
//
//  Created by Toan Truong on 03/12/2020.
//

import Foundation
import RxSwift

class HomeViewModel: BaseViewModel {
    private let repository: Repository
    
    init(repository: Repository) {
        self.repository = repository
    }
    
    func login(username: String, password: String) {
        loading.onNext(true)
        repository.login(username: username, password: password)
            .subscribe { [weak self](res) in
                self?.loading.onNext(false)
                self?.error.onNext(NSError(domain: String(describing: self), code: 0,
                                           userInfo: [NSLocalizedDescriptionKey: res?.error ?? "?"]))
            } onError: { [weak self](error) in
                self?.loading.onNext(false)
                self?.error.onNext(error as NSError)
            }
            .disposed(by: disposeBag)

    }
}
